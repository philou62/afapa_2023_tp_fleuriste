package Fleuriste;

public class Bouquet {

	protected String fleur;
	protected int nbreFleur;
	protected int qte;
	
	public Bouquet(String fleur, int nbreFleur, int qte) {
		super();
		this.fleur = fleur;
		this.nbreFleur = nbreFleur;
		this.qte = qte;
	}

	public String getFleur() {
		return fleur;
	}

	public void setFleur(String fleur) {
		this.fleur = fleur;
	}

	public int getNbreFleur() {
		return nbreFleur;
	}

	public void setNbreFleur(int nbreFleur) {
		this.nbreFleur = nbreFleur;
	}

	public int getQte() {
		return qte;
	}

	public void setQte(int qte) {
		this.qte = qte;
	}
	
	
	
	

}
