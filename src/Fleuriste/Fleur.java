package Fleuriste;

public class Fleur {
	
	private String nomFleur;
	private double prix;
	private int quantite;
	
	public Fleur(String nomFleur, double prix, int quantite) {
		super();
		this.nomFleur = nomFleur;
		this.prix = prix;
		this.quantite = quantite;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	
	public double getPrix() {
		return prix;
	}
	
	

	public String getNomFleur() {
		return nomFleur;
	}

	@Override
	public String toString() {
		return "Fleur [nom=" + nomFleur + ", prix=" + prix + ", quantite=" + quantite + "]";
	}

}