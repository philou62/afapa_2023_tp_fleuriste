package Fleuriste;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Fleuriste implements InterfaceFleuriste {
	Scanner sc = new Scanner(System.in);

	private Map<String, Fleur> stockFleurs = new HashMap<>();
	List<Fleur> fleur = new ArrayList<>();
	static List<String> cl = new ArrayList<>();
	List<Bouquet> bqt = new ArrayList<>();

	@Override
	public void creationBouquet(String nom, String prenom) {
		String nomPrenom = nom + " " + prenom;
		cl.add(nomPrenom);
	}

	@Override
	public int quantiteEnStock(String nomFleur) {
		if (stockFleurs.containsKey(nomFleur)) {
			return stockFleurs.get(nomFleur).getQuantite();
		} else {
			return 0;
		}
	}

	@Override
	public float prixDUneFleur(String nomFleur) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void ajoutFleur(String nomFleur, int Quantite) {
		// TODO Auto-generated method stub

	}

	@Override
	public void facturation() {
		// TODO Auto-generated method stub

	}

	public static void main(String[] args) {
		Fleuriste moi = new Fleuriste();
		System.out.println(moi.quantiteEnStock("rose"));
		moi.creationBouquet("Lamorski", "Philippe");
		moi.creationBouquet("Paul", "Personne");
		for (String String : cl) {
			System.out.println("Liste des clients => " + cl);
		}

		Fleur rose = new Fleur("rose", 4.25, 500);
		System.out.println(rose.getQuantite());
		Fleur tulipe = new Fleur("Tulipe", 2.25, 500);
		System.out.println(tulipe.getQuantite());
	}

}
